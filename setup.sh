#!/bin/bash

export PATH=$PATH:/home/ubuntu/.local/bin/
export AIRFLOW_HOME=/home/ubuntu/mlrs_ai/airflow
export HOME=/home/ubuntu/mlrs_ai

echo "export AIRFLOW_HOME=/home/ubuntu/mlrs_ai/airflow" >> ../.bashrc
echo "export HOME=/home/ubuntu/mlrs_ai" >> .bashr

if ( ! [ -a .install_log ] )
    then
	# echo "export AIRFLOW_HOME=/home/ubuntu/mlrs_ai/airflow" >> ../.bashrc
	# echo "export HOME=/home/ubuntu/mlrs_ai" >> .bashrc
	# echo "source /home/ubuntu/venv/bin/activate" >> ../.bashrc
	echo "" > .install_log

	sudo apt-get update
	sudo apt-get install -y docker
	sudo apt-get install -y python3-pip
	sudo pip3 install virtualenv
	virtualenv -p /usr/bin/python3 venv


	source venv/bin/activate
	pip install papermill matplotlib networkx tensorflow-model-analysis jinja2==2.10.0 werkzeug==0.14.1 tensorflow==1.13.1 tfx==0.13.0 docker apache-airflow
	pip install flask tensorboard

	airflow initdb
	mkdir airflow/data
	mkdir airflow/data/iris
	mkdir airflow/dags
	mkdir airflow/tfx
	cp data.csv airflow/data/iris/


fi
